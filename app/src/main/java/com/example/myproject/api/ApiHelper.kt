package com.example.myproject.api

class ApiHelper(private val apiService: CamsApiService) {

    suspend fun GetCams() = apiService.getCamsByGroup(groupId = "225")
}