package com.example.myproject.api

import com.example.myproject.CameraInfo
import retrofit2.http.*

interface CamsApiService {


    @GET("/api/get-group/{groupId}")
    suspend fun getCamsByGroup(
        @Header("Authorization") auth: String = "Bearer 0308e9c004bf42f35613121e197fadfc",
        @Path("groupId") groupId: String
    ): List<CameraInfo>

}