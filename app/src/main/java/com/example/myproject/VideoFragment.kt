package com.example.myproject

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavArgs
import androidx.navigation.fragment.navArgs
import com.example.myproject.databinding.ActivityMainBinding
import com.example.myproject.databinding.FragmentBlankBinding
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import kotlinx.android.synthetic.main.fragment_blank.*
import kotlinx.android.synthetic.main.fragment_blank.view.*
import kotlinx.coroutines.delay


class VideoFragment : Fragment(R.layout.fragment_blank) {
    val args: VideoFragmentArgs by navArgs()
    private lateinit var player: SimpleExoPlayer
    private val playBackPosition = 0
    var hls: String = ""
    private lateinit var exoView: View
    private lateinit var binding: FragmentBlankBinding
    private lateinit var camera: CameraInfo


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBlankBinding.inflate(inflater, container, false)
        exoView = binding.playerView
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        camera = args.arg
        binding.tvName.text = camera.camName
        binding.tvAddress.text = camera.address
        binding.tvCoordinates.text =
            "${camera.coordinates.longitude} ${camera.coordinates.latitude}"
        initPlayer()


    }

    private fun initPlayer() {

        hls = "https://cams.is74.ru${camera.hls ?: ""}"
        player = SimpleExoPlayer.Builder(requireContext()).build()
            .also { exoPlayer -> exoView.player_view.player = exoPlayer }
        val mediaItem = MediaItem.fromUri(hls)
        player.setMediaItem(mediaItem)
        player.prepare()
        player.play()

    }


}