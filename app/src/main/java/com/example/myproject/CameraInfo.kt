package com.example.myproject

import android.os.Parcelable
import androidx.annotation.ColorInt
import androidx.room.*
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "streets")
@Parcelize
data class CameraInfo(
    @SerializedName("OBJECT")
    @ColumnInfo(name = "objectType")
    val objectType: String,

    @PrimaryKey
    @SerializedName("ID")
    val camId: String,

    @SerializedName("NAME")
    @ColumnInfo(name = "camName")
    val camName: String,

    @SerializedName("SHORT_NAME")
    val shortName: String?,

    @SerializedName("PORCH")
    val porch: Int?,

    @SerializedName("HLS")
    @ColumnInfo(name = "hls")
    val hls: String?,

    @SerializedName("REALTIME_HLS")
    val hlsRealtime: String?,

    @Embedded
    @SerializedName("ARCHIVE")
    val archiveInfo: ArchiveModel?,

    @Embedded
    @SerializedName("SNAPSHOT")
    val snapshot: SnapshotModel,

    @Embedded
    @SerializedName("COORDINATES")
    val coordinates: CoordinatesCams,

    @SerializedName("ADDRESS")
    @ColumnInfo(name = "address")
    val address: String?,

    @Ignore
    @Embedded
    @SerializedName("ACCESS")
    val accessInfo: AccessCams?
) : Parcelable {
    constructor(objectType: String,
                camId: String,
                camName: String,
                shortName: String?,
                porch: Int?,
                hls: String?,
                hlsRealtime: String?,
                archiveInfo: ArchiveModel?,
                snapshot: SnapshotModel,
                coordinates: CoordinatesCams,
                address: String?): this(objectType,camId,camName,shortName,porch,hls,hlsRealtime,archiveInfo,snapshot,coordinates,address, null)
}

@Parcelize
data class CoordinatesCams(
    @SerializedName("LONGITUDE")
    val longitude: Double,

    @SerializedName("LATITUDE")
    val latitude: Double
) : Parcelable


@Parcelize
data class AccessCams(

    @SerializedName("LIVE")
    val liveAccess: AccessModel,


    @SerializedName("ARCHIVE")
    val archiveAccess: AccessModel,


    @SerializedName("DOWNLOAD")
    val downloadAccess: AccessModel,

    @SerializedName("MOVEMENT")
    val moveAccess: AccessModel
) : Parcelable

@Parcelize
data class AccessModel(
    @SerializedName("STATUS")
    val accessStatus: Boolean,

    @SerializedName("REASON")
    val accessReason: String
) : Parcelable

@Parcelize
data class ArchiveModel(
    @SerializedName("LINK")
    val link: String,

    @SerializedName("START_TIME")
    val startTime: String,

    @SerializedName("STOP_TIME")
    val stopTime: String
) : Parcelable

@Parcelize
data class SnapshotModel(
    @SerializedName("HD")
    val hd: String,
    @SerializedName("LOSSY")
    val lossy: String
) : Parcelable

