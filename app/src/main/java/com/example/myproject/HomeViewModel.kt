package com.example.myproject

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myproject.api.ApiHelper
import com.example.myproject.api.RetrofitBuilder
import com.example.myproject.database.AppDatabase
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {
    private val apiHelper = ApiHelper(RetrofitBuilder.apiService)
    private val appDatabase = AppDatabase.getDatabase()
    private val repository = Repository(apiHelper, appDatabase)

    private val _cameras = MutableLiveData<List<CameraInfo>?>(null)
    val cameras: LiveData<List<CameraInfo>?> = _cameras

    init {
        viewModelScope.launch {
            val result = repository.getCams()
            _cameras.postValue(result)
        }
    }

}