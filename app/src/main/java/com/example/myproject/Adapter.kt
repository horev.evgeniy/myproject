package com.example.myproject

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class Adapter(private val camera: ArrayList<CameraInfo>, val listener: OnSnapShotClickListener): RecyclerView.Adapter<Adapter.DataViewHolder>()  {

    class DataViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind (camera: CameraInfo, listener: OnSnapShotClickListener){
            val imSnapshot = itemView.findViewById<ImageView>(R.id.iv_snap)
            val tvName = itemView.findViewById<TextView>(R.id.tv_name)
            imSnapshot.setOnClickListener{
                listener.onClick(camera)
            }

            val url: String = "https://cams.is74.ru${camera.snapshot.lossy}"
            tvName.text = camera.camName
            Glide.with(imSnapshot.context)
                .load(url)
                .into(imSnapshot)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layout,parent,false)
        return DataViewHolder(view)

    }

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bind(camera[position], listener)
    }

    override fun getItemCount(): Int {
        return camera.size
    }

    interface OnSnapShotClickListener {
        fun onClick(camera: CameraInfo)
    }

    fun addCameras(camera: List<CameraInfo>){
        this.camera.apply {
            clear()
            addAll(camera)
        }
        notifyDataSetChanged()
    }
}