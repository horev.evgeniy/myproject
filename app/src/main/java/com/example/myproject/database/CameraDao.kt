package com.example.myproject.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.myproject.CameraInfo

@Dao
interface CameraDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCamera(camera: CameraInfo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllCameras(cameras: List<CameraInfo>)

    @Query("SELECT * FROM streets")
    suspend fun getAllCameras(): List<CameraInfo>?

    @Query("SELECT * FROM streets WHERE camId =:id")
    suspend fun getCamera(id: String) : CameraInfo
}