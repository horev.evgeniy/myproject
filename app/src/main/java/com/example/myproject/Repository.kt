package com.example.myproject

import com.example.myproject.api.ApiHelper
import com.example.myproject.database.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.DisposableHandle
import kotlinx.coroutines.withContext

class Repository(private val apiHelper: ApiHelper, appDatabase: AppDatabase?) {
    private val cameraDao = appDatabase?.cameraDao()

    suspend fun getCams(): List<CameraInfo>? = withContext(Dispatchers.IO){
       val apiResult =  apiHelper.GetCams()
        cameraDao?.insertAllCameras(apiResult)
        cameraDao?.getAllCameras()
    }

}